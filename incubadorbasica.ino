#include "DHT.h"
#include <EEPROM.h>
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>


#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 32 // OLED display height, in pixels

// Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)
#define OLED_RESET     4 // Reset pin # (or -1 if sharing Arduino reset pin)
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

#define NUMFLAKES     10 // Number of snowflakes in the animation example

#define LOGO_HEIGHT   16
#define LOGO_WIDTH    16
DHT dht;
///////////////////////////////////
//////////////Valores
int tempDeseada = EEPROM.read(0);
int humDeseada = EEPROM.read(1);
int dia = EEPROM.read(2);
int hora = EEPROM.read(3);
//int minutos = EEPROM.read(4);
int minutos =0;
/////////////////
#define tiempoRotacionHuevo 6*60*10 // 1=10 seg
#define releOn  0
#define releOff  1
#define TIEMPOLECDATOS 1000
#define TIEMPOREFRESCO 30*1000//tiempo entre actualizaciond e datos y control de clima 10*1000 = 10seg
#define TIEMPOMEDIA TIEMPOREFRESCO*50//tIEMPO PARA LA MEDIA TEMP
int temp, hum;
float temM,humM;
int ventilador = 5;
int huevos = 6;
int humificador = 7;
int resistencia = 9;
int bMode = 11;
int bMas = 10;
int bMenos = 12;
unsigned int contador = 0;
bool rot =false;
bool mode = 0;
int contMedia = 0;
char cont=0;

float mediaT ;
float mediaH ;
float mediaTemp =0;
float mediaHum = 0;
float mediaTempAc =0;
float mediaHumAc = 0;
int seg = 0;
unsigned long t1, t2, t3,t4,t5;

void setup() 
{
  Serial.begin(115200);
  dht.setup(3);                 // data pin 2
  pinMode(resistencia, OUTPUT);       //Rele 1
  pinMode(huevos, OUTPUT);       //Rele 2
  pinMode(humificador, OUTPUT); //humificador
  pinMode(ventilador, OUTPUT);  //Ventiladores
  pinMode(bMode, INPUT_PULLUP); //boton1
  pinMode(bMenos, INPUT_PULLUP); //boton2
  pinMode(bMas, INPUT_PULLUP); //boton3

  //apagamos todo
  digitalWrite(resistencia, LOW);
  digitalWrite(huevos, LOW);
  digitalWrite(ventilador, LOW);
  digitalWrite(humificador, LOW);
  ////////////////////////////////Pantalla
  // SSD1306_SWITCHCAPVCC = generate display voltage from 3.3V internally
  if(!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) { // Address 0x3C for 128x32
    Serial.println(F("SSD1306 allocation failed"));
    for(;;); // Don't proceed, loop forever
  }

  // Show initial display buffer contents on the screen --
  // the library initializes this with an Adafruit splash screen.
  display.display();
  delay(1000); // Pause for 2 seconds

  // Clear the buffer
  display.clearDisplay();

  // Draw a single pixel in white
  display.drawPixel(10, 10, BLACK);

  // Show the display buffer on the screen. You MUST call display() after
  // drawing commands to make them visible on screen!
  display.display();
  t1 = millis();
}

void loop()
{   
  t2 = millis();
  //lectura de dht para realizar una media a fin de ser mas preciso
  if(t2>(t1+TIEMPOLECDATOS))
  {
    getTempHum(&hum,&temp);
    temM += temp;
    humM += hum;
    cont ++;
    t1=millis();
     //Actualizamos el tiempo
    seg = seg + ((t2-t5)/1000);
    t5 = millis();
    if(seg>=59){seg = seg%60;minutos++;if(minutos>=59){minutos = 0; hora++;EEPROM.write( 3, hora );if(hora>=23){hora = 0;dia++;EEPROM.write( 2, dia);}}}
  }
  
  ////////TIEMPO DE REFRESCO CADA VEZ QUE PUEDAN CONMUTAR LOS RELES
  if(t2> (t3+TIEMPOREFRESCO)){
    temp= temM/cont;
    hum= humM/cont;
    controlClima(temp, tempDeseada, hum, humDeseada);
    mediaTemp += temp;
    mediaHum += hum;
    contMedia ++;
    temM = 0;
    humM=0;
    cont=0;
    t3 = millis();
    
    
  }
  //Tiempo en ir muestreando la media 
  
  if(t2> (t4+TIEMPOMEDIA)){
    
    mediaTempAc = mediaTemp/contMedia;
    mediaHumAc = mediaHum/contMedia;
    

    mediaTemp =0;
    mediaHum =0;
    contMedia = 0;


    t4=millis();
    }
 

  //Programación por horas
  if(hora == 0){ digitalWrite(huevos,releOn);}  //cada 00:00 empiezo a rotar huevos  
  if(hora == 1){digitalWrite(huevos,releOff);} // a las 04:00 termino de rotar huevos y asi a diario
  //Programación por horas
  if(hora == 6){ digitalWrite(huevos,releOn);}  //cada 00:00 empiezo a rotar huevos  
  if(hora == 7){digitalWrite(huevos,releOff);} // a las 04:00 termino de rotar huevos y asi a diario

  //Programación por horas
  if(hora == 12){ digitalWrite(huevos,releOn);}  //cada 00:00 empiezo a rotar huevos  
  if(hora == 13){digitalWrite(huevos,releOff);} // a las 04:00 termino de rotar huevos y asi a diario

  //Programación por horas
  if(hora == 18){ digitalWrite(huevos,releOn);}  //cada 00:00 empiezo a rotar huevos  
  if(hora == 19){digitalWrite(huevos,releOff);} // a las 04:00 termino de rotar huevos y asi a diario
 
  boton();
  mostrarDatos();
 
  

}
//Basura
/*
void media(float h,float t){
  
    mediaH =(mediaH+ h)/2;
    mediaT = (mediaT+t)/2;

}
*/
void mostrarDatos()
{
  /*
  //Puerto Serie
    Serial.print(" Temperatura actual--> ");
    Serial.print(temp);
    Serial.print(" Temperatura deseada--> ");
    Serial.println(tempDeseada);
    Serial.print(" Humedad actual--> ");
    Serial.print(hum);
    Serial.print(" Humedad deseada--> ");
    Serial.println(humDeseada);
    Serial.print(" Contador--> ");
    Serial.println(contador);
    Serial.print(" Mode--> ");
    Serial.println(mode);
  */
    //Pantalla
    display.setTextSize(1);
    display.setTextColor(WHITE);
    display.setCursor(0,0);
    display.print("T:");
    display.print(temp);
    display.setCursor(40,0);
    display.print("G:");
    display.println(tempDeseada);
    display.setCursor(80,0);
    display.print("MT:");
    display.print(mediaTempAc);
    display.setCursor(0,8);
    display.print("H:");
    display.print(hum);
    display.setCursor(40,8);
    display.print("G:");
    display.print(humDeseada);
    display.setCursor(80,8);
    display.print("MH:");
    display.print(mediaHumAc);
    /*
    display.setCursor(0,16);
    display.print("m:");
    display.print(mode);
    display.setCursor(16,16);
    display.print("C:");
    display.print(contador);
    */
    display.setCursor(0,16);
    display.print("D:");
    display.print(dia);
    display.setCursor(28,16);
    display.print("h:");
    display.print(hora);
    display.setCursor(56,16);
    display.print("m:");
    display.print(minutos);
    display.setCursor(84,16);
    display.print("s:");
    display.print(seg);
    

    display.display();
    display.clearDisplay();
}
void controlClima(int tempActual, int tempDeseada, int humActual, int humDeseada)
{
  
  if (tempActual  < tempDeseada && tempActual != 0)
  {
    setResistencia(1);
  }
  else
  {
    setResistencia(0);
  }
  if (humActual < humDeseada  && humActual != 0)
  {
    setHum(1);
  }
  else
    setHum(0);
}
void boton()
{
    if (digitalRead(bMode) == LOW){
        mode = !mode;
        int au = 0;
        while (digitalRead(11) == LOW)
        {au++;
        delay(1000);
        }
        if(au>=5){dia = 1; hora = 0;minutos = 0; seg = 0;EEPROM.write( 2, dia); EEPROM.write( 3, hora );  }
    }
    if(mode == 0){
        if (digitalRead(bMenos) == LOW){
            //hago algo
            tempDeseada--;
            EEPROM.write( 0, tempDeseada );  //Grabamos el valor
            delay(500);
        }
        if (digitalRead(bMas) == LOW){
            //hago algo
            tempDeseada++;
            EEPROM.write( 0, tempDeseada );  //Grabamos el valor
            delay(500);
        }
    }else{
        if (digitalRead(bMenos) == LOW){
            //hago algo
            humDeseada--;
            EEPROM.write( 1, humDeseada );  //Grabamos el valor
            delay(500);
        }
        if (digitalRead(bMas) == LOW){
            //hago algo
            humDeseada++;
            EEPROM.write(1, humDeseada );  //Grabamos el valor
            delay(500);
        }
    }
}
void getTempHum(int  *h, int *t)
{
  *h = (int)dht.getHumidity();
  *t = (int)dht.getTemperature();
  if (isnan((int)h) || isnan((int)t) || h == 0 || t == 0)
  {
    Serial.println("Failed to read from DHT sensor!");
    return;
  }
 
}

void setResistencia(boolean b){
  if (b == 1)
  {
    //Serial.println("Encendemos Calor ");
    digitalWrite(ventilador, HIGH);
    digitalWrite(resistencia, releOn);
  }
  else 
  {
    //Serial.println("Apagamos Calor ");
    digitalWrite(ventilador, LOW);
    digitalWrite(resistencia, releOff);
  }
  
}

void setHum(boolean b){
  if (b == 1)
  {
    digitalWrite(humificador, releOn);
    //Serial.println("Enciendo humificador");
  }
  else
  {
    digitalWrite(humificador, releOff);
    //Serial.println("Apago humificador");
  }
}
